import { CalendarComponent } from 'ionic2-calendar';
import { Storage } from '@ionic/storage';
import { Component, ViewChild, OnInit, Inject, LOCALE_ID } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { formatDate } from '@angular/common';
import * as moment from 'moment';
import 'moment/locale/es'  // without this line it didn't work
import { title } from 'process';
import { Plugins, LocalNotification, LocalNotificationActionPerformed } from '@capacitor/core';
const { LocalNotifications } = Plugins;
moment.locale('es')
@Component({
  selector: "app-tab1",
  templateUrl: "tab1.page.html",
  styleUrls: ["tab1.page.scss"],
})
export class Tab1Page {
  private baseURL = 'https://animatiomx.com/lumi/';
  store: any;
  citas: any = [];
  event = {
    title: '',
    desc: '',
    startTime: '',
    endTime: '',
    allDay: false,
    eventColor: ''
  };

  minDate = new Date().toISOString();

  eventSource = [];
  viewTitle;

  calendar = {
    mode: 'month',
    currentDate: new Date(),
    locale: 'es-MX'
  };

  @ViewChild(CalendarComponent) myCal: CalendarComponent;

  constructor(private alertCtrl: AlertController, @Inject(LOCALE_ID) private locale: string, private http: HttpClient,
              private storage: Storage, private router: Router) { }

  ngOnInit() {
    this.resetEvent();
    this.storage.get('idL').then((id) => {
      this.store = id;
      console.log(this.store);
    });
    LocalNotifications.requestPermission();
    LocalNotifications.addListener('localNotificationActionPerformed', (payload) => {
      console.log(payload.actionId);
      const route = payload.notification.extra.route;
      this.router.navigate(route);
      // this.obtenerifocita(payload.actionId);
});

this.deshabilitaRetroceso();
  }

   deshabilitaRetroceso() {
    window.location.hash = 'no-back-button';
    window.location.hash = 'Again-No-back-button' //chrome
    window.onhashchange = function () { window.location.hash = 'no-back-button'; }
  }

  ionViewWillEnter()
  {
    this.citasT(this.store);
    this.ngOnInit();
  }

  resetEvent() {
    this.event = {
      title: '',
      desc: '',
      startTime: new Date().toISOString(),
      endTime: new Date().toISOString(),
      allDay: false,
      eventColor: ''
    };
  }

  addEvent() {
    this.eventSource = this.citas;
    this.myCal.loadEvents();
    this.resetEvent();
  }

citasT(user){
  const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
  const options: any = { 'caso': 18 , 'idUsuarioS': user};
  const URL: any = this.baseURL + 'calendario.php';
  this.http.post(URL, JSON.stringify(options), headers).subscribe(
    respuesta => {
      this.citas = respuesta;
      for (var i = 0; i < this.citas.length; i++) {
        this.citas[i].startTime = new Date(this.citas[i].startTime);
        this.citas[i].endTime = new Date(this.citas[i].endTime);
       }
      console.log(this.citas);
      this.addEvent();
    });
}
next() {
  var swiper = document.querySelector('.swiper-container')['swiper'];
  swiper.slideNext();
}

back() {
  var swiper = document.querySelector('.swiper-container')['swiper'];
  swiper.slidePrev();
}

// Change between month/week/day
changeMode(mode) {
  this.calendar.mode = mode;
}

// Focus today
today() {
  this.calendar.currentDate = new Date();
}

// Selected date reange and hence title changed
onViewTitleChanged(title) {
  this.viewTitle = title;
}

// Calendar event was clicked
async onEventSelected(endTime,idc,title,status) {
  console.log(event)

  var fechafinal = moment(endTime).format("YYYY-MM-DD");

  var fechahoy = moment().format("YYYY-MM-DD");

  console.log(fechafinal)

  var idcita = idc;

  var empresa = title;

  var status = status;

  console.log('mi fecha' + fechafinal);

  if (fechafinal === fechahoy && status === '1') {

    const alert = await this.alertCtrl.create({
      header: '¿Qué desea hacer?',
      buttons: [
        {
          text: 'Editar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Editar Cita');
            this.router.navigate(['/tabs/tab1/detalle-cita/' + idcita]);
          }
        }, {
          text: 'Ver detalle',
          handler: () => {
            console.log('Ir a detalle');
            this.router.navigate(['/tabs/tab1/mapa-cita/'+String(fechafinal)+'/'+empresa+'/'+idcita]);
          }
        }
      ]
    });
    alert.present();

  } else {
    this.router.navigate(['/tabs/tab1/mapa-cita/'+String(fechafinal)+'/'+empresa+'/'+idcita]);
  }
}

async onEventSelected1(event) {
  console.log(event)
  // Use Angular date pipe for conversion
  let start = formatDate(event.startTime, 'medium', this.locale);
  let end = formatDate(event.endTime, 'medium', this.locale);

  var fechafinal = moment(event.endTime).format("YYYY-MM-DD");

  var fechahoy = moment().format("YYYY-MM-DD");

  console.log(fechafinal)

  var idcita = event.idCitas;

  var empresa = event.title;

  var status = event.Status;

  console.log('mi fecha' + fechafinal);

  if (fechafinal === fechahoy && status === '1') {

    const alert = await this.alertCtrl.create({
      header: '¿Qué desea hacer?',
      buttons: [
        {
          text: 'Editar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Editar Cita');
            this.router.navigate(['/tabs/tab1/detalle-cita/' + event.idCitas]);
          }
        }, {
          text: 'Ver detalle',
          handler: () => {
            console.log('Ir a detalle');
            this.router.navigate(['/tabs/tab1/mapa-cita/'+String(fechafinal)+'/'+empresa+'/'+idcita]);
          }
        }
      ]
    });
    alert.present();

  } else {
    this.router.navigate(['/tabs/tab1/mapa-cita/'+String(fechafinal)+'/'+empresa+'/'+idcita]);
  }
}

// Time slot was clicked
onTimeSelected(ev) {
  let selected = new Date(ev.selectedTime);
  this.event.startTime = selected.toISOString();
  selected.setHours(selected.getHours() + 1);
  this.event.endTime = (selected.toISOString());
}
}