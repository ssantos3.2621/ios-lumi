import { Component, OnInit,ElementRef, ViewChild } from "@angular/core";
import { HttpHeaders } from "@angular/common/http";
import { HttpClient } from "@angular/common/http";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { ServicesService } from "../services/services.service";
import { Storage } from '@ionic/storage';
import { AuthenticationService } from '../services/auth.service';
import { ActionSheetController } from "@ionic/angular";
import { Plugins, CameraResultType, CameraSource, FilesystemDirectory, Capacitor } from "@capacitor/core";
import { LoadingController, ToastController } from "@ionic/angular";
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { catchError, finalize } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Upload } from 'tus-js-client';
const { LocalNotifications } = Plugins;
const { Camera, Filesystem } = Plugins;

@Component({
  selector: "app-tab4",
  templateUrl: "tab4.page.html",
  styleUrls: ["tab4.page.scss"],
})
export class Tab4Page implements OnInit {
  private baseURL = "https://animatiomx.com/lumi/";
  public url_servidor = "https://animatiomx.com/lumi/uploadfoto.php";
  usuario: any = [];
  prueba: any = [];
  equipo: any = [];
  equipo2: any = [];
  ide = "";
  idUsuario = "";
  info = [];
  selectedFile: null;
  nombrefoto: any;
  visible = false;
  shown: boolean;
  foto = "";
  submitted = false;
  returnUrl: string;
  fotoFinal: any;
  fileData: File = null;
  previewUrl: SafeResourceUrl;
  public respuestaImagenEnviada;
  public resultadoCarga;
  myphoto: any;
  files: FileList;
  fotoguardar = "";
  counter = 0;
  fotodeperfil: any;
  public error: string;
  private loader: any;

  @ViewChild("MyRef", { static: false }) MyRef: ElementRef;

  constructor(
    private authService: AuthenticationService,
    public storage: Storage,
    private enviandoImagen: ServicesService,
    public route: ActivatedRoute,
    private http: HttpClient,
    public actionSheetController: ActionSheetController,
    private router: Router,
    private sanitizer: DomSanitizer,
    public loadingCtrl: LoadingController,
    private transfer: FileTransfer,
    private readonly toastCtrl: ToastController
  ) {
    const info1 = localStorage.getItem("idL");
    const info2 = info1.replace("[", "").replace("]", "").replace("", "");
    this.info = info2.split(",");
    this.idUsuario = this.info[0].replace('"', "").replace('"', "");
    console.log(this.idUsuario);
  }

  ngOnInit() {
    this.obtenerusuario();
    this.obteneridequipo();
    LocalNotifications.requestPermission();
    // LocalNotifications.addListener('localNotificationReceived', (notification) => {
    //   console.log('Notification: ', notification);
    //   this.presentAlert(1, 2);
    // });
    // LocalNotifications.addListener('localNotificationReceived', (notification: LocalNotification) => {
    //   this.presentAlert(`${notification.title}`, `${JSON.stringify(notification.extra)}`);
    //   this.cosa1 = `${notification.title}`;
    //   this.cosa2 = `${JSON.stringify(notification.extra)}`;
    //   console.log(`${notification.title}`, `${JSON.stringify(notification.extra)}`, 'hola');
    // });
    LocalNotifications.addListener('localNotificationActionPerformed', (payload) => {
      console.log(payload.actionId);
      const route = payload.notification.extra.route;
      this.router.navigate(route);
      // this.obtenerifocita(payload.actionId);
});
this.deshabilitaRetroceso();
  }

  deshabilitaRetroceso() {
    window.location.hash = 'no-back-button';
    window.location.hash = 'Again-No-back-button' //chrome
    window.onhashchange = function () { window.location.hash = 'no-back-button'; }
  }

  // Previsualiza la imagen
  preview() {
    var mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
    var reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = (_event) => {
      this.previewUrl = reader.result;
    };
  }

  // Obtiene el nombre y cambia el mismo de la foto subida
  onFileChanged(event, files: FileList, fileInput: any) {
    const Numero1 = Math.floor(Math.random() * 10001).toString();
    const Numero2 = Math.floor(Math.random() * 1001).toString();
    // Nombre del archivo
    this.selectedFile = event.target.files[0];
    this.nombrefoto = event.target.files[0].name;
    this.foto = Numero1 + Numero2 + this.nombrefoto.replace(/ /g, "");
    console.log(this.foto);
    this.fotoFinal = files;
    this.fileData = <File>fileInput.target.files[0];
    this.preview();
    this.cargandoImagen(this.fotoFinal, this.foto);
    this.updatefoto(this.foto);
  }
  // Sube imagen a servidor
  public cargandoImagen(files: FileList, nombre: string) {
    this.enviandoImagen.postFileImagen(files[0], nombre).subscribe(
      (response) => {
        this.respuestaImagenEnviada = response;
        if (this.respuestaImagenEnviada <= 1) {
          console.log("Error en el servidor");
        } else {
          if (
            this.respuestaImagenEnviada.code == 200 &&
            this.respuestaImagenEnviada.status == "success"
          ) {
            this.resultadoCarga = 1;
          } else {
            this.resultadoCarga = 2;
          }
        }
      },
      (error) => {
        console.log(<any>error);
      }
    ); // FIN DE METODO SUBSCRIBE
  }

  obtenerusuario() {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 0, idUsuario: this.idUsuario };
    const URL: any = this.baseURL + "perfil.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.usuario = respuesta;
      });
  }

  obteneridequipo() {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 1, idUsuario: this.idUsuario };
    const URL: any = this.baseURL + "perfil.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.equipo = respuesta;
        this.ide = this.equipo.length ? this.equipo[0].Equipos_idEquipos : null;
        console.log(this.ide);

        this.obtenerequipo(this.ide);
      });
  }

  obtenerequipo(ide) {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = {
      caso: 3,
      idEquipo: ide,
      idUsuario: this.idUsuario,
    };
    const URL: any = this.baseURL + "perfil.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.equipo2 = respuesta;
        console.log(this.equipo2);
      });
  }

  updatefoto(foto) {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = {
      caso: 2,
      foto: foto,
      idUsuario: this.idUsuario,
    };
    const URL: any = this.baseURL + "perfil.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        console.log("Done");
      });
  }

  activar(id) {
    console.log(id);
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 1, idUsuario: id };
    const URL: any = this.baseURL + "usuario.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.obtenerequipo(this.ide);
      });
  }
  desactivar(id) {
    console.log(id);
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 2, idUsuario: id };
    const URL: any = this.baseURL + "usuario.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.obtenerequipo(this.ide);
      });
  }

  logout() {
    this.storage.clear();
    this.authService.logout();
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: "Abrir",
      buttons: [
        {
          text: "Album",
          icon: "image-outline",
          handler: () => {
            // this.takePicture();
            this.MyRef.nativeElement.click();
          },
        },
        {
          text: "Tomar foto",
          icon: "camera-outline",
          handler: () => {
            this.takePhoto();
          },
        },
        {
          text: "Cancelar",
          icon: "close",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          },
        },
      ],
    });
    await actionSheet.present();
  }

  // async takePicture() {
  //   const image = await Camera.getPhoto({
  //     quality: 70,
  //     allowEditing: false,
  //     source: CameraSource.Camera,
  //     resultType: CameraResultType.Uri,
  //   });

  async takePhoto() {
    const options = {
      quality: 70,
      allowEditing: false,
      source: CameraSource.Camera,
      resultType: CameraResultType.Uri,
    };

    const originalPhoto = await Camera.getPhoto(options);
    // const photoInTempStorage = await Filesystem.readFile({ path: originalPhoto.path });

    // image.webPath will contain a path that can be set as an image src.
    // You can access the original file using image.path, which can be
    // passed to the Filesystem API to read the raw data of the image,
    // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)

    this.previewUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
      originalPhoto && originalPhoto.webPath
    );

    this.uploadAll(originalPhoto.webPath);
    // this.myphoto = 'data:image/jpeg;base64,' + originalPhoto.base64String;
    // this.uploadPhoto();
  }

  private async uploadAll(webPath: string) {
    this.loader = await this.loadingCtrl.create({
      message: "subiendo imagen...",
    });
    await this.loader.present();

    const blob = await fetch(webPath).then((r) => r.blob());

    var random = Math.floor(Math.random() * 100);
    var random1 = Math.floor(Math.random() * 100);

    this.foto = "imagen-perfil" + random + random1 + ".jpg";
    this.updatefoto(this.foto);

    const formData = new FormData();
    formData.append("imagenPropia", blob, this.foto);
    this.http
      .post<boolean>("https://animatiomx.com/lumi/uploadfoto1.php", formData)
      .pipe(
        catchError((e) => this.handleError(e)),
        finalize(() => this.loader.dismiss())
      )
      .subscribe((ok) => this.showToast(true));
  }

  uploadPhoto(): Promise<any> {
    let diayfecha = new Date().toISOString();
    let fecha = diayfecha
      .substr(0, diayfecha.length - 14)
      .replace("-", "")
      .replace("-", "");
    let fecha1 = fecha.substr(2, fecha.length);
    let hora = diayfecha
      .substr(11, diayfecha.length)
      .replace(":", "")
      .replace(":", "")
      .replace(".", "");
    let hora1 = hora.substr(0, hora.length - 4);

    var random = Math.floor(Math.random() * 100);
    var random1 = Math.floor(Math.random() * 100);



    return new Promise((resolve, reject) => {
      const fileTransfer: FileTransferObject = this.transfer.create();
      const options: FileUploadOptions = {
        mimeType: "image/jpeg",
        chunkedMode: false,
        fileKey: "photo",
        fileName: "imagen_" + fecha1 + "_" + random + hora1 + random1 + ".jpg",
        httpMethod: "post",
        params: {
          //any params that you want to attach for the server to use
        },
        headers: {
          //any specific headers go here
        },
      };
      fileTransfer
        .upload(
          this.myphoto,
          "https://animatiomx.com/lumi/uploadimagen.php",
          options
        )
        .then(
          (result) => {
            this.fotoguardar = result.response;
            this.updatefoto(this.fotoguardar);
            resolve(result);
          },
          (err) => {
            this.error = err;
            reject(err);
          }
        );
    });
  }

  private async showToast(ok: boolean) {
    if (ok) {
      const toast = await this.toastCtrl.create({
        message: "imagen subida correctamente",
        duration: 3000,
        position: "top",
      });
      toast.present();
    } else {
      const toast = await this.toastCtrl.create({
        message: "fallo al subir imagen",
        duration: 3000,
        position: "top",
      });
      toast.present();
    }
  }

  private handleError(error: any) {
    const errMsg = error.message ? error.message : error.toString();
    this.error = errMsg;
    return throwError(errMsg);
  }
}
