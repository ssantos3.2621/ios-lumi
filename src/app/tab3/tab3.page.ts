import { Component, ViewChild, OnInit, EventEmitter, Output,
  AfterViewInit, Input} from '@angular/core';
//<reference types="@types/googlemaps" />
//import {} from '@types/googlemaps';
import { AlertController } from '@ionic/angular';
import { formatDate } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SubirarchivoService } from 'src/app/services/subirarchivo.service';
import { LoadingController, ToastController } from '@ionic/angular';
import { Geolocation} from '@capacitor/core';
import { Plugins, LocalNotification, LocalNotificationActionPerformed } from '@capacitor/core';
const { LocalNotifications } = Plugins;
import { ActivatedRoute, Params, Router } from "@angular/router";
@Component({
  selector: "app-tab3",
  templateUrl: "tab3.page.html",
  styleUrls: ["tab3.page.scss"],
})
export class Tab3Page implements OnInit, AfterViewInit {
  fecha;
  private baseURL = "https://animatiomx.com/lumi_app/";
  prospecto: any = [];
  autocompleteInput = '';
  archivos: string[] = [];
  nombredearchivos: any = [];
  idarchivo: any = [];
  tipodearchivo: any = [];
  nombrear = "";
  previewUrl: any = [];
  idUsuario;
  info = [];

  @ViewChild("content", { static: false }) content: any;
  @ViewChild("emp", { static: false }) emp: any;
  @ViewChild("addresstext", { static: false }) addresstext: any;
  @ViewChild("rfc", { static: false }) rfc: any;
  @Input() adressType: string;
  @Output() setAddress: EventEmitter<any> = new EventEmitter();
  @ViewChild("contact", { static: false }) contact: any;
  @ViewChild("phone", { static: false }) phone: any;
  @ViewChild("email", { static: false }) email: any;
  @ViewChild("coment", { static: false }) coment: any;

  ngAfterViewInit() {
    this.direccionactual();
    this.getPlaceAutocomplete();
  }

  private getPlaceAutocomplete() {
    const autocomplete = new google.maps.places.Autocomplete(
      this.addresstext.nativeElement,
      {
        componentRestrictions: { country: "MX" },
        types: [this.adressType], // 'establishment' / 'address' / 'geocode'
      }
    );
    google.maps.event.addListener(autocomplete, "place_changed", () => {
      const place = autocomplete.getPlace();
      this.invokeEvent(place);
    });
  }

  invokeEvent(place: Object) {
    this.setAddress.emit(place);
  }

  async direccionactual(){
    const posicion = await Geolocation.getCurrentPosition();
    const latitud = posicion.coords.latitude;
    const longitud = posicion.coords.longitude;
    var geocoder = new google.maps.Geocoder();
    var coordenadas = new google.maps.LatLng(latitud, longitud);
    geocoder.geocode({ 'latLng': coordenadas}, function(results, status){
      if(status == google.maps.GeocoderStatus.OK) {
        var direccion=results[0].formatted_address;
        this.addresstext = direccion;
        this.autocompleteInput = direccion;
        (<HTMLInputElement>document.getElementById('searchinput')).value = direccion;
        console.log(this.addresstext);
      }else{
        console.log('error')
      }
    });
  }
  constructor(
    public loadingCtrl: LoadingController,
    private subirarchivo: SubirarchivoService,
    public alertController: AlertController,
    private http: HttpClient,
    private router: Router
  ) {
    const info1 = localStorage.getItem("idL");
    const info2 = info1.replace("[", "").replace("]", "").replace("", "");
    this.info = info2.split(",");
    this.idUsuario = this.info[0].replace('"', "").replace('"', "");
    console.log(this.idUsuario);
  }

  ngOnInit() {
    this.fecha = formatDate(new Date(), "yyyy-MM-dd hh:mm:ss", "es");
    LocalNotifications.requestPermission();
    // LocalNotifications.addListener('localNotificationReceived', (notification) => {
    //   console.log('Notification: ', notification);
    //   this.presentAlert(1, 2);
    // });
    // LocalNotifications.addListener('localNotificationReceived', (notification: LocalNotification) => {
    //   this.presentAlert(`${notification.title}`, `${JSON.stringify(notification.extra)}`);
    //   this.cosa1 = `${notification.title}`;
    //   this.cosa2 = `${JSON.stringify(notification.extra)}`;
    //   console.log(`${notification.title}`, `${JSON.stringify(notification.extra)}`, 'hola');
    // });
    LocalNotifications.addListener('localNotificationActionPerformed', (payload) => {
      console.log(payload.actionId);
      const route = payload.notification.extra.route;
      this.router.navigate(route);
      // this.obtenerifocita(payload.actionId);
});
this.deshabilitaRetroceso();
  }

  deshabilitaRetroceso() {
    window.location.hash = 'no-back-button';
    window.location.hash = 'Again-No-back-button' //chrome
    window.onhashchange = function () { window.location.hash = 'no-back-button'; }
  }

  async guardarprospecto() {
    if (this.emp.value == "") {
      this.presentAlert3();
    } else if (this.addresstext.nativeElement.value == "") {
      this.presentAlert4();
    } else {
      const headers: any = new HttpHeaders({
        "Content-Type": "application/json",
      });
      let loader = await this.loadingCtrl.create({
        message: "Guardando prospecto",
      });
      await loader.present();

      const options: any = {
        caso: 3,
        idU: this.idUsuario,
        Namepros: this.emp.value,
        Dirpros: this.addresstext.nativeElement.value,
        rfc: this.rfc.value,
        Contacpros: this.contact.value,
        Telpros: this.phone.value,
        email: this.email.value,
        comentario: this.coment.value,
        fecha1: this.fecha,
      };
      const URL: any = this.baseURL + "tareas.php";
      this.http
        .post(URL, JSON.stringify(options), headers)
        .subscribe((respuesta) => {
          const idtarea = respuesta.toString();
          if (this.previewUrl.length === 0) {
            loader.dismiss();
            this.presentAlert();
          } else {
            const formData = new FormData();
            for (var i = 0; i < this.archivos.length; i++) {
              formData.append("file[]", this.archivos[i]);
            }
            formData.append("idEmp", idtarea);
            formData.append("idtarea", "0");
            formData.append("idcita", "0");

            this.subirarchivo.enviarimagen(formData).subscribe((resp) => {
              if (resp.toString() !== "") {
                loader.dismiss();
                this.nombredearchivos = [];
                this.archivos = [];
                this.previewUrl = [];
                this.presentAlert();
              } else {
                loader.dismiss();
                this.nombredearchivos = [];
                this.archivos = [];
                this.previewUrl = [];
                this.presentAlert2();
              }
            });
          }
        });
    }
  }

  inicializarcariables() {
    this.archivos = [];
    this.previewUrl = [];
    this.nombredearchivos = [];
    this.tipodearchivo = [];
  }

  // onFileChanged(event) {
  //   for (var i = 0; i < event.target.files.length; i++) {
  //     let reader = new FileReader();
  //     reader.onload = (event: any) => {
  //       this.previewUrl.push(event.target.result);
  //     };
  //     reader.readAsDataURL(event.target.files[i]);

  //     this.archivos.push(event.target.files[i]);
  //     this.nombredearchivos.push(event.target.files[i].name);
  //     var ext = event.target.files[i].name
  //       .substring(event.target.files[i].name.lastIndexOf(".") + 1)
  //       .toLowerCase();
  //     this.tipodearchivo.push(ext);
  //   }
  //   for (let index = 0; index < event.target.files[0].name.length; index++) {
  //     this.nombredearchivos.push(event.target.files[0].name[index]);
  //   }
  //   console.log(this.nombredearchivos);
  //   console.log(this.archivos);
  //   console.log(this.tipodearchivo);
  // }
  onFileChanged(event) {
    for (var i = 0; i < event.target.files.length; i++) {
      this.archivos.push(event.target.files[i]);
      this.nombredearchivos.push(event.target.files[i].name);
      var ext = event.target.files[i].name
        .substring(event.target.files[i].name.lastIndexOf(".") + 1)
        .toLowerCase();
      this.tipodearchivo.push(ext);
    }
    console.log(this.nombredearchivos);
    console.log(this.archivos);
    console.log(this.tipodearchivo);
  }
  quitararchivo(i) {
    this.archivos.splice(i, 1);
    this.previewUrl.splice(i, 1);
    this.nombredearchivos.splice(i, 1);
    this.tipodearchivo.splice(i, 1);
    console.log(this.nombredearchivos);
    console.log(this.archivos);
    console.log(this.tipodearchivo);
  }

  servidorarchivo(idtarea) {
    const formData = new FormData();
    for (var i = 0; i < this.archivos.length; i++) {
      formData.append("file[]", this.archivos[i]);
    }
    formData.append("idEmp", idtarea);
    formData.append("idtarea", "0");
    formData.append("idcita", "0");

    this.subirarchivo.enviararchivo(formData).subscribe((resp) => {
      if (resp.toString() !== "") {
        this.nombredearchivos = [];
        this.archivos = [];
        this.previewUrl = [];
      } else {
        console.log("ocurrio un error");
        this.nombredearchivos = [];
        this.archivos = [];
      }
    });
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: "alertclass",
      header: "Tu prospecto se guardado correctamente",
      buttons: ["Entendido"],
    });
    (this.emp.value = ""),
      (this.addresstext.nativeElement.value = ""),
      (this.contact.value = ""),
      (this.phone.value = ""),
      (this.email.value = ""),
      (this.coment.value = ""),
      (this.rfc.value = "");
    await alert.present();
  }

  async presentAlert2() {
    const alert = await this.alertController.create({
      cssClass: "alertclass",
      header: "Ocurrio un error intentelo nuevamente",
      buttons: ["Entendido"],
    });
    await alert.present();
  }

  async presentAlert3() {
    const alert = await this.alertController.create({
      cssClass: "alertclass",
      header: "Debe agregar un nombre",
      buttons: ["Entendido"],
    });
    await alert.present();
  }

  async presentAlert4() {
    const alert = await this.alertController.create({
      cssClass: "alertclass",
      header: "Debe agregar una dirección",
      buttons: ["Entendido"],
    });
    await alert.present();
  }
}
 declare var google;