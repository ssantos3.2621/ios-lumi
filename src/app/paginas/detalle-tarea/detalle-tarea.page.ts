import { Component, OnInit, ViewChild } from "@angular/core";
import { HttpHeaders } from "@angular/common/http";
import { HttpClient } from "@angular/common/http";
import {
  FormControl,
  FormBuilder,
  FormGroup,
  Validators,
  NgForm,
} from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { SubirarchivoService } from "src/app/services/subirarchivo.service";
import { FormsModule } from "@angular/forms";
import { AlertController } from '@ionic/angular';
import { formatDate } from '@angular/common';
import { registerLocaleData } from '@angular/common';
import { LOCALE_ID, NgModule } from '@angular/core';
import localePy from '@angular/common/locales/es-PY';
import { LoadingController, ToastController } from '@ionic/angular';


registerLocaleData(localePy, 'es');

@Component({
  selector: "app-detalle-tarea",
  templateUrl: "./detalle-tarea.page.html",
  styleUrls: ["./detalle-tarea.page.scss"],
})
export class DetalleTareaPage implements OnInit {
  private baseURL = "https://animatiomx.com/lumi_app/";
  private baseURL2 = "https://animatiomx.com/lumi/";
  idTarea: number;
  tarea: any = [];
  rem: any = [];
  arch: any = [];
  com: any = [];
  archivos: string[] = [];
  nombredearchivos: any = [];
  tipodearchivo: any = [];
  info = [];
  Comentario = "";
  idUsuario;
  status = "1";
  cancel = "";
  idEmpresas = '';
  Tarea = '';
  responsable = '';
  responsableARealizar = '';
  idEmpresasS = '';
  sucursal = '';
  fecha;
  nocancel=true;

  constructor(
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient,
    private subirarchivo: SubirarchivoService,
    public alertController: AlertController,
    public FormsModule: FormsModule
  ) {
    this.route.params.subscribe((parametros) => {
      this.idTarea = parametros["idT"];
    });

    const info1 = localStorage.getItem("idL");
    const info2 = info1.replace("[", "").replace("]", "").replace("", "");
    this.info = info2.split(",");
    this.idUsuario = this.info[0].replace('"', "").replace('"', "");
    console.log(this.idUsuario);
  }
  ngOnInit() {
    this.fecha = formatDate(new Date(), "yyyy-MM-dd hh:mm:ss", "es");
    console.log(this.fecha);
    this.obtenertarea();
    this.obtenerRemitente(this.idTarea);
    this.obtenerArchivos(this.idTarea);
    this.obtenerComentarios(this.idTarea);
  }

  obtenertarea() {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 1, idT: this.idTarea };
    const URL: any = this.baseURL + "tareas.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.tarea = respuesta;
        for (let i = 0; i < this.tarea.length; i++) {
          const element = this.tarea[i];
          this.Tarea = this.tarea[0].Tarea;
          this.responsable = this.tarea[0].Users_idUsers;
          this.responsableARealizar = this.tarea[0].idResponsable;
          this.idEmpresasS = this.tarea[0].idEmpresas;
          this.sucursal = this.tarea[0].Sucursal;
          this.idEmpresas = this.tarea[0].idEmpresas;
          console.log(this.Tarea);
        }
        console.log(this.tarea);
      });
  }

  obtenerRemitente(id) {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 14, idT: id };
    const URL: any = this.baseURL + "tareas.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.rem = respuesta;
        console.log(this.rem);
      });
  }

  obtenerComentarios(id) {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 15, idT: id };
    const URL: any = this.baseURL + "tareas.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.com = respuesta;
      });
  }

  obtenerArchivos(id) {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 16, idT: id };
    const URL: any = this.baseURL + "tareas.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.arch = respuesta;
      });
  }

  eliminaArchivos(id) {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 5, idA: id };
    const URL: any = this.baseURL2 + "tareas.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.arch = respuesta;
      });
    this.ngOnInit();
  }

  async InsertaPregunta() {
    // tslint:disable-next-line:one-variable-per-declaration
    let loader = await this.loadingCtrl.create({
      message: "Actualizando tarea",
    });
    await loader.present();

    const headers: any = new HttpHeaders({
        "Content-Type": "application/json",
      }),
      options: any = {
        caso: 3.1,
        'idR': this.responsableARealizar,
        'idS': this.sucursal,
        tarea: this.Tarea,
        idT: this.idTarea,
        comentario: this.Comentario,
        idU: this.idUsuario,
        status: this.status,
        fecha1:this.fecha
      },
      URL: any = this.baseURL2 + "tareas.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {});

    if (this.archivos.length === 0) {
      loader.dismiss();
      this.ngOnInit();
      this.Comentario = "";
      this.cancel = "";
      this.nombredearchivos = [];
      this.archivos = [];
      this.presentAlertrue();
    } else {
      const formData = new FormData();
      for (var i = 0; i < this.archivos.length; i++) {
        formData.append("file[]", this.archivos[i]);
      }
      formData.append("idtarea", this.idTarea.toString());
      formData.append("idcita", "0");
      formData.append("idEmp", "0");
      this.subirarchivo.enviararchivo(formData).subscribe((resp) => {
        if (resp.toString() !== "") {
          loader.dismiss();
          this.ngOnInit();
          this.presentAlertrue();
          this.nombredearchivos = [];
          this.archivos = [];
        } else {
          loader.dismiss();
          this.presentAlerterror();
          this.nombredearchivos = [];
          this.archivos = [];
        }
      });
    }


  }
cancelar(){
this.nocancel=false;
}
  CancelaPregunta() {
    if (this.cancel!='') {
      console.log(this.cancel);
      // tslint:disable-next-line:one-variable-per-declaration
      const headers: any = new HttpHeaders({
        "Content-Type": "application/json",
      }),
        options: any = {
          caso: 3.2,
          'idR': this.responsableARealizar,
          'idS': this.sucursal,
          tarea: this.Tarea,
          idT: this.idTarea,
          idU: this.idUsuario,
          status: 3,
          cancel: this.cancel,
          fecha1: this.fecha
        },
        URL: any = this.baseURL2 + "tareas.php";
      this.http
        .post(URL, JSON.stringify(options), headers)
        .subscribe((respuesta) => { });
      this.presentAlercancel();
      this.cancel = "";
      this.nocancel = true;
    }else{
      this.presentAlertcancel();
    }
  }

  onFileChanged(event) {
    for (var i = 0; i < event.target.files.length; i++) {
      this.archivos.push(event.target.files[i]);
      this.nombredearchivos.push(event.target.files[i].name);
      var ext = event.target.files[i].name
        .substring(event.target.files[i].name.lastIndexOf(".") + 1)
        .toLowerCase();
      this.tipodearchivo.push(ext);
    }
    // for (let index = 0; index < event.target.files[0].name.length; index++) {
    //   this.nombredearchivos.push(event.target.files[0].name[index]);
    // }
    console.log(this.nombredearchivos);
    console.log(this.archivos);
    console.log(this.tipodearchivo);
  }

  quitararchivo(i) {
    this.archivos.splice(i, 1);
    this.nombredearchivos.splice(i, 1);
    this.tipodearchivo.splice(i, 1);
    console.log(this.nombredearchivos);
    console.log(this.archivos);
    console.log(this.tipodearchivo);
  }

  servidorarchivo(idT) {
    const formData = new FormData();
    for (var i = 0; i < this.archivos.length; i++) {
      formData.append("file[]", this.archivos[i]);
    }
    formData.append("idtarea", idT);
    formData.append("idcita", "0");
    formData.append("idEmp", "0");
    this.subirarchivo.enviararchivo(formData).subscribe((resp) => {
      if (resp.toString() !== "") {
        this.ngOnInit();
        this.presentAlertrue();
        this.nombredearchivos = [];
        this.archivos = [];
      } else {
        this.presentAlerterror();
        console.log("ocurrio un error");
        this.nombredearchivos = [];
        this.archivos = [];
      }
    });
  }

  async presentAlertcancel() {
    const alert = await this.alertController.create({
      cssClass: 'alertclass',
      header: 'Error! debe agregar un motivo',
      buttons: ['Entendido']
    });
    await alert.present();
  }


  async presentAlertrue() {
    const alert = await this.alertController.create({
      cssClass: 'alertclass',
      header: 'Se guardo la tarea correctamente',
      buttons: [
        {
          text: 'Entendido',
          handler: () => {
            this.router.navigate(['/tabs/tab2']);
          }
        }
      ]
    });
    await alert.present();
  }

  async presentAlercancel() {
    const alert = await this.alertController.create({
      cssClass: 'alertclass',
      header: 'La tarea se cancelo',
      buttons: [
        {
          text: 'Entendido',
          handler: () => {
            this.router.navigate(['/tabs/tab2']);
          }
        }
      ]
    });
    await alert.present();
  }

  async presentAlerterror() {
    const alert = await this.alertController.create({
      cssClass: 'alertclass',
      header: 'Ocurrio un error intentelo de nuevo',
      buttons: ['Entendido']
    });
    await alert.present();
  }

}
