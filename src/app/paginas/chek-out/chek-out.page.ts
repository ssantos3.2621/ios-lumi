import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { ToastController } from '@ionic/angular';
import * as moment from 'moment';
import 'moment/locale/es';

@Component({
  selector: 'app-chek-out',
  templateUrl: './chek-out.page.html',
  styleUrls: ['./chek-out.page.scss'],
})
export class ChekOutPage implements OnInit {
  private baseURL = 'https://animatiomx.com/lumi/';
  idcita = '';
  nombreE = '';
  checkin = '';
  horainput = '';
  horainput1 = '';
  horaa: number;
  minutoa: number;
  horaa1 = '';
  minutoa1 = '';
  contadortimer;
  citas: any = [];
  citas1: any = [];
  idcheckin = '';

  constructor(
    private router: Router, 
    private route: ActivatedRoute,
    private http: HttpClient,
    public toastCtrl: ToastController
  ) { }

  ngOnInit() {
    this.idcita = this.route.snapshot.paramMap.get('idC');
    this.nombreE = this.route.snapshot.paramMap.get('NomE');
    this.checkin = this.route.snapshot.paramMap.get('checkI');
    this.idcheckin = this.route.snapshot.paramMap.get('idCheck');
  }
  ionViewWillEnter() {
    clearInterval(this.contadortimer);
    this.idcita = this.route.snapshot.paramMap.get('idC');
    this.nombreE = this.route.snapshot.paramMap.get('NomE');
    this.checkin = this.route.snapshot.paramMap.get('checkI');
    this.idcheckin = this.route.snapshot.paramMap.get('idCheck');
    this.contador();
    this.historialcitas(this.idcheckin);
  }
  ionViewDidLeave(){
    clearInterval(this.contadortimer);
  }

  hora(){
    const horaactual = new Date();

    this.horainput = horaactual.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });

    this.horaa = horaactual.getHours();
    this.minutoa = horaactual.getMinutes();

    if (this.horaa <=9) {

      this.horaa1 = '0' + this.horaa;
      
    }else{
      this.horaa1 = String(this.horaa);
    }

    if (this.minutoa <= 9) {
      this.minutoa1 = '0' + this.minutoa;
    }else{
      this.minutoa1 = String(this.minutoa);
    }

    this.horainput = this.horaa1 + ":" + this.minutoa1 + " Hrs ";

    this.horainput1 = this.horaa1 + ":" + this.minutoa1;

    console.log(this.horainput);
  }

  contador(){
    this.contadortimer = setInterval(() => {
      this.hora();
     }, 1000);
  }

  checkout(){
    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { 'caso': 5, 'Horafinal': this.horainput1, 'idCitas': this.idcita};
    const URL: any = this.baseURL + 'check.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        const res = respuesta;
        if (res.toString() === '1') {
          this.router.navigate(['/tabs/tab1']);
          this.mensaje('Cita terminada correctamente');
        } else {
          this.mensaje('Intenta nuevamente');
        }
      });
  }

  async mensaje(msg) {
    let toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000,
      cssClass: "toastcurva",
    });

    await toast.present();

}

historialcitas(idCheck){
  const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
  const options: any = { 'caso': 9, 'idC': idCheck };
  const URL: any = this.baseURL + 'check.php';
  this.http.post(URL, JSON.stringify(options), headers).subscribe(
    respuesta => {
      this.citas1 = respuesta;
      if (this.citas1.length > 0) {
        this.citas = respuesta;
      }
      
    });
}


async mensaje1(asesor,motivo,fechai,horai) {
  let toast = await this.toastCtrl.create({
    message: 'Asesor: '+asesor+'\nCita: '+motivo+'\nFecha: '+(moment(fechai).format('LL'))+'\nHora: '+horai,
    duration: 3000,
    position: 'top',
    cssClass: "toastcurva",
  });

  await toast.present();

}


}
