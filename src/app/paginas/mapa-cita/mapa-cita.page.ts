import { Component, OnInit } from '@angular/core';
import { ActionSheetController, LoadingController, Platform,ToastController } from '@ionic/angular';
import { Geolocation} from '@capacitor/core';
import { Media,MediaObject } from '@ionic-native/media/ngx';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpHeaders, HttpClient } from '@angular/common/http';
//import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { BackgroundGeolocation, BackgroundGeolocationConfig,BackgroundGeolocationEvents, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation/ngx';
import { Plugins } from '@capacitor/core';
const { LocalNotifications } = Plugins;
declare var google;
import * as moment from 'moment';
import 'moment/locale/es';
interface Marker {
  position: {
    lat: number,
    lng: number,
  };
  title: string;
}

@Component({
  selector: 'app-mapa-cita',
  templateUrl: './mapa-cita.page.html',
  styleUrls: ['./mapa-cita.page.scss'],
})
export class MapaCitaPage implements OnInit {
  private baseURL = 'https://animatiomx.com/lumi/';
  map = null;
  latitud: number;
  longitud: number;
  total: string = 'Calculando...';
  latitudi: string;
  longitudi: string;
  contadorposicion;
  contadornotificacion;
  audio: MediaObject;
  check: boolean = false;
  metros = 0;
  fechahoy = moment().format("YYYY-MM-DD  h:mm a");
  fechacita ='';
  idcita = '';
  infocita: any = [];
  direccion = '';
  fecha = '';
  motivo = '';
  horaI = '';
  horaF = '';
  fechaI = '';
  idE = '';
  status = '';
  vermasdetalle: boolean = false;
  visitas: any = [];
  nombreempresa = '';
  verseguimiento: boolean = false;
  iniciarseguimiento: boolean = false;
  estatuscita = '';
  archivosadjuntos: any = [];

  config: BackgroundGeolocationConfig = {
    desiredAccuracy: 10,
    stationaryRadius: 20,
    distanceFilter: 30,
    debug: false, //  Esto hace que el dispositivo emita sonidos cuando lanza un evento de localización
    stopOnTerminate: false, // Si pones este en verdadero, la aplicación dejará de trackear la localización cuando la app se haya cerrado.
    //Estas solo están disponibles para Android
    locationProvider: 1, //Será el proveedor de localización. Gps, Wifi, Gms, etc...
    startForeground: true,
    interval: 1000, //El intervalo en el que se comprueba la localización.
    fastestInterval: 1000, //Este para cuando está en movimiento.
    activitiesInterval: 1000, //Este es para cuando está realizando alguna actividad con el dispositivo.
};

  constructor(
    public actionSheetController: ActionSheetController,
     public loading: LoadingController,
     private media: Media,
     private router: Router,
     private route: ActivatedRoute,
     private http: HttpClient,
     private plt: Platform,
     public toastCtrl: ToastController,
     //private backgroundMode: BackgroundMode,
     private backgroundGeolocation: BackgroundGeolocation
     ) {
      // this.plt.ready().then(() => {
      //     this.backgroundMode.enable();
      //     this.backgroundMode.setDefaults({ title: "Centinela segundo plano.",text:''});
      //     this.backgroundMode.on('activate').subscribe(() => {
      //       this.backgroundMode.disableWebViewOptimizations();
      //     });

      //     backgroundMode.on('deactivate').subscribe(() => {
      //       })

      //     });

    this.backgroundGeolocation.configure(this.config)
  .then(() => {
    this.backgroundGeolocation.on(BackgroundGeolocationEvents.location).subscribe((location: BackgroundGeolocationResponse) => {
      this.latitud = location.latitude;
      this.longitud = location.longitude;
      this.cordenadasiniciales();
      // Es muy importante llamar al método finish() en algún momento para que se le notifique al sistema que hemos terminado y que libere lo que tenga que liberar,
      // Si no se hace, la aplicación dará error por problemas de memoria.
      //this.backgroundGeolocation.finish(); // SOLO PARA IOS
    });
  });
// Inicia el trackig de geolocalización

// Y este es para terminar de trackear la geolocalización.
//this.backgroundGeolocation.stop();
   }

   ngOnInit() {
    console.log('ngOnInit')
    var ruta = 'https://animatiomx.com/lumi/Archivos/sonido.mp3';
    this.audio = this.media.create(ruta);
    this.fechacita = this.route.snapshot.paramMap.get('fc');
    this.idcita = this.route.snapshot.paramMap.get('idc');
    this.nombreempresa = this.route.snapshot.paramMap.get('nemp');
    // console.log('fecha del dia de hoy'+this.fechahoy)
    // console.log('fecha del dia de la cita'+this.fechacita)
    // console.log('id de la cita'+this.idcita)
    this.cargandomapa();
  }

  ionViewWillEnter() {

    console.log('ionViewWillEnter')
  }

  ionViewDidEnter(){
     console.log('ionViewDidEnter')

    this.check = false;
    this.metros = 0;
    this.total = 'Calculando...'
    this.vermasdetalle = false;
    this.iniciarseguimiento = false;
    if (moment(this.fechacita).format("YYYY-MM-DD  h:mm a") >= moment(this.fechahoy).format("YYYY-MM-DD  h:mm a")) {
      this.verseguimiento = true;
    }
    setTimeout(() => {
      //this.cordenadasiniciales();
      //this.backgroundGeolocation.start();
      this.obtenerubicacion1();
      this.obtenervisitas();
      this.obtenerarchivos();
     }, 1500);

  }
  ionViewDidLeave(){
    console.log('ionViewDidLeave')
    clearInterval(this.contadornotificacion);
    clearInterval(this.contadorposicion);
    this.backgroundGeolocation.stop();

  }

  async cargandomapa() {
    const cargando = await this.loading.create({
      message: "Cargando mapa..."
    });

    await cargando.present();

    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { 'caso': 0 , 'idCitas': this.idcita};
    const URL: any = this.baseURL + 'detalle_cita.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.infocita = respuesta;
        this.direccion = respuesta[0].Direccion;
        this.status = respuesta[0].Status_Empresa;
        console.log(this.status);
        this.motivo = respuesta[0].Motivo;
        this.fechaI = respuesta[0].FechaI;
        this.fecha = (moment(respuesta[0].FechaI).format('dddd DD [de] MMMM [de] YYYY'));
        this.horaI = respuesta[0].HoraI;
        this.horaF = respuesta[0].HoraF;
        this.idE= respuesta[0].idEmpresa;
        this.estatuscita = respuesta[0].Status;
        if (this.estatuscita === '2') {
          cargando.dismiss();
          this.router.navigate(['/tabs/tab1/chek-in/'+ this.idcita]);
        } else {
          var geocoder = new google.maps.Geocoder();
          //var direcc = 'LA JAROCHA, Río Lerma Norte, Cuautlancingo, Rafael Ávila Camacho (Manantiales, Santiago Momoxpan, Puebla';
          geocoder.geocode({ 'address': this.direccion}, function(results, status){
            if (status == google.maps.GeocoderStatus.OK)
            {
                  var lat = results[0].geometry.location.lat();
                  var long = results[0].geometry.location.lng();

                  this.latitudi = lat;
                  this.longitudi = long;
                  (<HTMLInputElement>document.getElementById('lat')).value = lat;
                  (<HTMLInputElement>document.getElementById('long')).value = long;

                  const elementomapa: HTMLElement = document.getElementById('mapa');
                  const obtenercordenadas = {lat: this.latitudi, lng: this.longitudi};
                  this.map = new google.maps.Map(elementomapa, {
                    center: obtenercordenadas,
                    zoom: 17,
                    disableDefaultUI: true,
                    streetViewControl: true,
                    mapTypeControl: false,
                    scaleControl: true,
                    zoomControl: true,
                    fullscreenControl: true
                  });

                  google.maps.event.addListenerOnce(this.map, 'idle', () => {
                    elementomapa.classList.add('show-map');
                    const marker = {
                      position: {
                        lat: this.latitudi,
                        lng: this.longitudi
                      }
                    }
                    new google.maps.Marker({
                      position: marker.position,
                      map: this.map,
                      animation: google.maps.Animation.BOUNCE,
                    });
                    cargando.dismiss();
                  });

            } else {
              cargando.dismiss();
              
           }
          });
        }
      });
  }
  
  async mensaje1(msg){
    let toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top',
      cssClass: "toastcurva",
    });

    await toast.present();
  }
  cordenadasiniciales(){
    if (this.fechahoy < this.fechacita) {
        this.obtenerubicacion();
        this.notificacion();
    } else {
      console.log('la fecha no es hoy');
    }
  }

  async obtenerubicacion() {
    this.calculateDistance(this.longitud, this.longitudi, this.latitud, this.latitudi);
  }

  async obtenerubicacion1() {
    const posicion = await Geolocation.getCurrentPosition();
    this.latitud = posicion.coords.latitude;
    this.longitud = posicion.coords.longitude;
    this.calculateDistance(this.longitud, this.longitudi, this.latitud, this.latitudi);
   }

  calculateDistance(lon1, lon2, lat1, lat2){

    let total = '0 KM';

    var posicion1 = new google.maps.LatLng(lat1, lon1);
    var posicion2 = new google.maps.LatLng(lat2, lon2);

    console.log('posicion i'+posicion1);
    console.log('posicion f'+posicion2);

    var distancia = google.maps.geometry.spherical.computeDistanceBetween(posicion1, posicion2);
    var dis = distancia/ 1000;

    var metros1 = 0;

    if (dis < 1) {

      const distancia = String(dis).split('.');

      const metros = distancia[1].substr(0,3);

      if (Number(metros) < 100) {
        this.total = metros.substr(-2) + ' m';
        metros1 = Number(metros.substr(-2));
        this.metros = metros1;
        if (metros1 <= 99) {
          if (moment(this.fechacita).format("YYYY-MM-DD  h:mm a") >= moment(this.fechahoy).format("YYYY-MM-DD  h:mm a")) {
          this.check = true;
          }
          if (metros1 >= 70) {
            this.audio.play();
          }
        }else{
          this.check = false;
        }
      } else {
        this.total = metros + ' m';
        metros1 =Number(metros);
        this.metros = metros1;
      }

    } else {
      this.total = String(dis).substr(0,4)+ ' km';
      this.check = false;
      this.metros = 100;
    }
}

  notificacion(){
    this.total = this.total;
    (<HTMLInputElement>document.getElementById('totalfinal')).value = this.total;
    this.iniciarseguimiento = true;
      LocalNotifications.schedule({
        notifications: [
          {
            title: this.total,
            body: "Para llegar a tu destino",
            id: 1,
          }
        ]
      });
  }

  obtenervisitas() {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 0.1, 'idCitas': this.idcita, 'fecha': this.fechaI, 'idEmp':this.idE };
    const URL: any = this.baseURL + 'detalle_cita.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.visitas = respuesta;
      });
  }

  obtenerarchivos() {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 7, 'idCitas': this.idcita };
    const URL: any = this.baseURL + 'check.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.archivosadjuntos = respuesta;
      });
  }

  expandircard(){
    if (!this.vermasdetalle) {
      this.vermasdetalle = true
    } else {
      this.vermasdetalle = false
    }
  }

  iralpunto(){
    this.iniciarseguimiento = true;
    this.backgroundGeolocation.start();
    LocalNotifications.schedule({
      notifications: [
        {
          title: this.total,
          body: "Para llegar a tu destino",
          id: 1,
        }
      ]
    });
  }

  checkin(){
    const horaactual = moment().format('HH:mm');
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 0, 'idCitas': this.idcita, 'HoraInicio': horaactual };
    const URL: any = this.baseURL + 'check.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        try {
          this.backgroundGeolocation.stop();
          clearInterval(this.contadornotificacion);
          clearInterval(this.contadorposicion);
          this.backgroundGeolocation.stop();
          this.router.navigate(['/tabs/tab1/chek-in/'+ this.idcita]);
        } catch (error) {
          console.log('hubo un error' + error);
        }
      });
  }

  eliminaArchivos(id) {
    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { 'caso': 7, 'idArchivo': id};
    const URL: any = this.baseURL + 'calendario.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
    respuesta => {
      this.obtenerarchivos();
    });
  }

  async mensaje(asesor,motivo) {
    let toast = await this.toastCtrl.create({
      message: 'Asesor: '+asesor+'\nMotivo: '+motivo,
      duration: 3000,
      position: 'top',
      cssClass: "toastcurva",
    });

    await toast.present();

}

}