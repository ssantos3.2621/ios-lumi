import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MapaCitaPageRoutingModule } from './mapa-cita-routing.module';

import { MapaCitaPage } from './mapa-cita.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MapaCitaPageRoutingModule
  ],
  declarations: [MapaCitaPage]
})
export class MapaCitaPageModule {}
