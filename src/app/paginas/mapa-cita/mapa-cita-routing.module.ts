import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MapaCitaPage } from './mapa-cita.page';

const routes: Routes = [
  {
    path: '',
    component: MapaCitaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MapaCitaPageRoutingModule {}
