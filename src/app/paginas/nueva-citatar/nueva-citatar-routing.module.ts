import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NuevaCitatarPage } from './nueva-citatar.page';

const routes: Routes = [
  {
    path: '',
    component: NuevaCitatarPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NuevaCitatarPageRoutingModule {}
