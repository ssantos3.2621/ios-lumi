import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormControl, FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { formatDate } from '@angular/common';
import { Storage } from '@ionic/storage';
import { SubirarchivoService } from 'src/app/services/subirarchivo.service';
import { AlertController } from '@ionic/angular';
import { LoadingController, ToastController } from '@ionic/angular';
import { IonicSelectableComponent } from 'ionic-selectable';
@Component({
  selector: "app-nueva-tarea",
  templateUrl: "./nueva-tarea.page.html",
  styleUrls: ["./nueva-tarea.page.scss"],
})
export class NuevaTareaPage implements OnInit {
  fecha;
  empresa: any = [];
  sucursales: any = [];
  usuarios: any = [];
  archivos: string[] = [];
  nombredearchivos: any = [];
  tipodearchivo: any = [];
  idUsuario;
  info = [];
  users="";
  private baseURL = "https://animatiomx.com/lumi_app/";
  private baseURL2 = "https://animatiomx.com/lumi/";

  @ViewChild("emp", { static: false }) emp: any;
  @ViewChild("sucursal", { static: false }) sucursal: any;
  @ViewChild("user", { static: false }) user: any;
  @ViewChild("tarea", { static: false }) tarea: any;

  constructor(
    public loadingCtrl: LoadingController,
    public alertController: AlertController,
    private http: HttpClient,
    private subirarchivo: SubirarchivoService,
    private storage: Storage,
  ) {
    const info1 = localStorage.getItem("idL");
    const info2 = info1.replace("[", "").replace("]", "").replace("", "");
    this.info = info2.split(",");
    this.idUsuario = this.info[0].replace('"', "").replace('"', "");
    console.log(this.idUsuario);
  }

  ngOnInit() {
    this.fecha = formatDate(new Date(), "yyyy-MM-dd hh:mm:ss", "es");
    this.obtenerempresa(this.idUsuario);
    this.obtenerusuario(this.idUsuario);
  }

  obtenerempresa(idu) {
    console.log(idu);
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 1, 'idUsuario': idu };
    const URL: any = this.baseURL2 + "calendario.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.empresa = respuesta;
        console.log(this.empresa);
      });
  }

  obtenersucursal(event: { component: IonicSelectableComponent,value: any}) {
    console.log(event.value.idEmpresas)
    this.emp = event.value.idEmpresas;
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 5, idE: this.emp };
    const URL: any = this.baseURL + "tareas.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.sucursales = respuesta;

        console.log(this.sucursales);
      });
  }

  obtenerusuario(idu) {
    console.log(idu);
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 12, 'idUs': idu };
    const URL: any = this.baseURL2 + "detalle_cita.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.usuarios = respuesta;
        console.log(this.usuarios);
      });
  }

  async guardartarea() {
    console.log(
      this.emp,
      this.sucursal.value,
      this.users,
      this.tarea.value,
      this.fecha
    );
    if (this.emp == undefined) {
      this.presentAlert();
    } else if (this.sucursal.value == undefined) {
      this.presentAlert2();
    } else if (this.users == undefined) {
      this.presentAlert3();
    } else if (this.tarea.value == "") {
      this.presentAlert4();
    } else {
      let loader = await this.loadingCtrl.create({
        message: "Creando Tarea",
      });
      await loader.present();

      const headers: any = new HttpHeaders({
        "Content-Type": "application/json",
      });
      const options: any = {
        caso: 7,
        idE: this.emp,
        suc: this.sucursal.value,
        idRes: this.users,
        tarea: this.tarea.value,
        fecha1: this.fecha,
        idU: this.idUsuario,
      };
      const URL: any = this.baseURL + "tareas.php";
      this.http
        .post(URL, JSON.stringify(options), headers)
        .subscribe((respuesta) => {
          const idtarea = respuesta.toString();
          if (this.archivos.length === 0) {
            loader.dismiss();
            (this.emp = ""),
            (this.sucursal.value = ""),
            (this.users = ""),
            (this.tarea.value = "");
            this.presentAlertrue();
          } else {
            const formData = new FormData();
            for (var i = 0; i < this.archivos.length; i++) {
              formData.append("file[]", this.archivos[i]);
            }
            formData.append("idtarea", idtarea);
            formData.append("idEmp", this.emp);
            formData.append("idcita", "0");

            this.subirarchivo.enviararchivo(formData).subscribe((resp) => {
              if (resp.toString() !== "") {
                loader.dismiss();
                this.nombredearchivos = [];
                this.archivos = [];
                (this.emp = ""),
                (this.sucursal.value = ""),
                (this.users = ""),
                (this.tarea.value = "");
                this.presentAlertrue();
              } else {
                loader.dismiss();
                this.nombredearchivos = [];
                this.archivos = [];
                this.presentAlerError();
              }
            });
          }
        });
    }
  }

  onFileChanged(event) {
    for (var i = 0; i < event.target.files.length; i++) {
      this.archivos.push(event.target.files[i]);
      this.nombredearchivos.push(event.target.files[i].name);
      var ext = event.target.files[i].name
        .substring(event.target.files[i].name.lastIndexOf(".") + 1)
        .toLowerCase();
      this.tipodearchivo.push(ext);
    }
    // for (let index = 0; index < event.target.files[0].name.length; index++) {
    //   this.nombredearchivos.push(event.target.files[0].name[index]);
    // }
    console.log(this.nombredearchivos);
    console.log(this.archivos);
    console.log(this.tipodearchivo);
  }

  quitararchivo(i) {
    this.archivos.splice(i, 1);
    this.nombredearchivos.splice(i, 1);
    this.tipodearchivo.splice(i, 1);
    console.log(this.nombredearchivos);
    console.log(this.archivos);
    console.log(this.tipodearchivo);
  }

  servidorarchivo(idtarea) {
    const formData = new FormData();
    for (var i = 0; i < this.archivos.length; i++) {
      formData.append("file[]", this.archivos[i]);
    }
    formData.append("idtarea", idtarea);
    formData.append("idEmp", this.emp.value);
    formData.append("idcita", "0");

    this.subirarchivo.enviararchivo(formData).subscribe((resp) => {
      if (resp.toString() !== "") {
        this.nombredearchivos = [];
        this.archivos = [];
      } else {
        console.log("ocurrio un error");
        this.nombredearchivos = [];
        this.archivos = [];
      }
    });
  }

  async presentAlertrue() {
    const alert = await this.alertController.create({
      cssClass: 'alertclass',
      header: 'Se guardo la tarea correctamente',
      buttons: ['Entendido']
    });
    await alert.present();
  }

  async presentAlerError() {
    const alert = await this.alertController.create({
      cssClass: 'alertclass',
      header: 'Ocurrio un error intentelo de nuevo',
      buttons: ['Entendido']
    });
    await alert.present();
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'alertclass',
      header: 'Debe seleccionar una empresa',
      buttons: ['Entendido']
    });
    await alert.present();
  }

  async presentAlert2() {
    const alert = await this.alertController.create({
      cssClass: 'alertclass',
      header: 'Debe seleccionar una sucursal',
      buttons: ['Entendido']
    });
    await alert.present();
  }

  async presentAlert3() {
    const alert = await this.alertController.create({
      cssClass: 'alertclass',
      header: 'Debe seleccionar un colaborador',
      buttons: ['Entendido']
    });
    await alert.present();
  }
  async presentAlert4() {
    const alert = await this.alertController.create({
      cssClass: 'alertclass',
      header: 'Debe agregar una descripción',
      buttons: ['Entendido']
    });
    await alert.present();
  }
}
