import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { SubirarchivoService } from 'src/app/services/subirarchivo.service';
import { AlertController } from '@ionic/angular';
import * as moment from 'moment';
import { Plugins, LocalNotification, LocalNotificationActionPerformed } from '@capacitor/core';
const { LocalNotifications } = Plugins;
import { IonicSelectableComponent } from 'ionic-selectable';

@Component({
  selector: 'app-nueva-cita',
  templateUrl: './nueva-cita.page.html',
  styleUrls: ['./nueva-cita.page.scss'],
})
export class NuevaCitaPage implements OnInit {
  cosa1: any;
  cosa2: any;
  negado = '';
  idempresa = '';
  empresasinfo: any = [''];
  empresasinfo2: any = [''];
  recordatorioinfO: any = [];
  idrecordatorio = '';
  sucuO: any = [];
  csuc = true;
  cprospecto = true;
  prospecto = true;
  direccion = '';
  idUltima: any = [];
  fechaelegi: any;
  fechaelegi2: any;
  fecha2: any;
  fecha3: any;
  fecha4: any;
  tiempo: any;
  Motivo = '';
  idUsuario = '';
  idSO = '';
  fechainicio;
  fechafinal;
  horainicial;
  horafinal;
  fechayhorainicial;
  fechayhorafinal;
  nombreE2 = '';
  store: any;
  empresasid: any = [];
  archivos: string[] = [];
  nombredearchivos: any = [];
  tipodearchivo: any = [];
  fechafinall: any;
  empresa: any;
  idcita: any;
  private baseURL = 'https://animatiomx.com/lumi/';
  constructor(private http: HttpClient, private storage: Storage, private subirarchivo: SubirarchivoService,
              public alertController: AlertController, private router: Router ) { }

  ngOnInit() {
    this.storage.get('idL').then((id) => {
      this.store = id;
      console.log(this.store);
      this.empresas(this.store);
      this.recordatorio();
    });
    LocalNotifications.requestPermission();
    // LocalNotifications.addListener('localNotificationReceived', (notification) => {
    //   console.log('Notification: ', notification);
    //   this.presentAlert(1, 2);
    // });
    // LocalNotifications.addListener('localNotificationReceived', (notification: LocalNotification) => {
    //   this.presentAlert(`${notification.title}`, `${JSON.stringify(notification.extra)}`);
    //   this.cosa1 = `${notification.title}`;
    //   this.cosa2 = `${JSON.stringify(notification.extra)}`;
    //   console.log(`${notification.title}`, `${JSON.stringify(notification.extra)}`, 'hola');
    // });
    LocalNotifications.addListener('localNotificationActionPerformed', (payload) => {
      console.log(payload.actionId);
      const route = payload.notification.extra.route;
      this.router.navigate(route);
      // this.obtenerifocita(payload.actionId);
});
  }
  obtenerifocita(id){
    let fechaurl;
    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { caso: 21, idCita: id};
    const URL: any = this.baseURL + 'calendario.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.empresasinfo2 = respuesta;
        this.empresa = this.empresasinfo2[0].Nombre_Empresa;
        this.idcita  = this.empresasinfo2[0].idCitas;
        this.fechafinall = this.empresasinfo2[0].endTime;
        fechaurl = moment(this.fechafinall).format('YYYY-MM-DD  h:mm a');
      });
    this.router.navigate(['/tabs/tab1/mapa-cita/'+String(fechaurl)+'/'+this.empresa+'/'+this.idcita]);
    this.presentAlert(fechaurl, this.empresa);
  }

  async presentAlert(dato1, dato2) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: dato1,
      subHeader: 'Subtitle',
      message: dato2,
      buttons: ['OK']
    });

    await alert.present();
  }

  empresas(idUsuario){
    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { 'caso': 1, 'idUsuario': idUsuario};
    const URL: any = this.baseURL + 'calendario.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.empresasid = respuesta;
      });
  }

  obtenerifoempresa(event: { component: IonicSelectableComponent,value: any}){
    console.log('actividad:', event.value.idEmpresas);
    this.idempresa = event.value.idEmpresas;
    this.cprospecto = false;
    if (this.idempresa !== ''){
    }
    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { 'caso': 3, 'idEmpresa': this.idempresa};
    const URL: any = this.baseURL + 'calendario.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.empresasinfo = respuesta;
        this.direccion = this.empresasinfo[0].Direccion_Empresa;
        this.nombreE2 = this.empresasinfo[0].Nombre_Empresa;
        console.log(this.nombreE2);
        console.log(this.direccion);
      });
    this.obtenSucO();
  }

  obtenSucO() {
    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { 'caso': 16, 'idEmp': this.idempresa};
    const URL: any = this.baseURL + 'detalle_cita.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.sucuO = respuesta;
        if (this.sucuO != null) {
          this.csuc = true;
        }else{
          this.csuc = false;
        }
      });
  }
  obtenerifoempresa1(event: { component: IonicSelectableComponent,value: any}){
    console.log('actividad:', event.value.idEmpresas);
    this.idempresa = event.value.idEmpresas;
    this.prospecto = false;
    this.idSO = '0';
    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { 'caso': 3, 'idEmpresa': this.idempresa};
    const URL: any = this.baseURL + 'calendario.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.empresasinfo = respuesta;
        this.direccion = this.empresasinfo[0].Direccion_Empresa;
        this.nombreE2 = this.empresasinfo[0].Nombre_Empresa;
      });
    this.obtenSucO();
  }
  recordatorio(){
    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { 'caso': 2};
    const URL: any = this.baseURL + 'calendario.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.recordatorioinfO = respuesta;
      });
  }
  onFileChanged(event) {
    for (var i = 0; i < event.target.files.length; i++) {
      this.archivos.push(event.target.files[i]);
      this.nombredearchivos.push(event.target.files[i].name);
      var ext = event.target.files[i].name
        .substring(event.target.files[i].name.lastIndexOf(".") + 1)
        .toLowerCase();
      this.tipodearchivo.push(ext);
    }
  }
quitararchivo(i) {
  this.archivos.splice(i, 1);
  this.nombredearchivos.splice(i, 1);
  this.tipodearchivo.splice(i, 1);
  console.log(this.nombredearchivos);
  console.log(this.archivos);
  console.log(this.tipodearchivo);
}
fecha(){
  const fecha = new Date();
  const fechahoy = moment(fecha).format();
  const fechaelegida =  moment(this.fechainicio + ' ' + this.horainicial).format();
  console.log(fechahoy);
  console.log(fechaelegida);
  if (fechahoy > fechaelegida) {
    this.fechayhorainicial = '';
  } else {
    this.fechayhorainicial = (moment(this.fechainicio).format('dddd DD [de] MMMM [de] YYYY')) + ' ' + this.horainicial;
    this.fechaelegi = this.fechainicio.concat('T' + this.horainicial.toString());
    this.fechaelegi2 = new Date(this.fechaelegi);
    // Tiempo es igual a un dia en microsecs
    if (this.idrecordatorio === '1')
    {
      this.tiempo = 1000 *  60 * 5;
    }
    if (this.idrecordatorio === '2')
    {
      this.tiempo = 1000 *  60 * 15;
    }
    if (this.idrecordatorio === '3')
    {
      this.tiempo = 1000 *  60 * 30;
    }
    if (this.idrecordatorio === '4')
    {
      this.tiempo = 1000 *  60 * 60 * 24 * 1;
    }
    if (this.idrecordatorio === '5')
    {
      this.tiempo = 1000 *  60 * 60 * 24 * 2;
    }
    // getTime convierte una fecha a microsegundos
    this.fecha2 = this.fechaelegi2.getTime();
    // Operacion para quitar el tiempo a la fecha asignada
    this.fecha3 = this.fecha2 - this.tiempo;
    console.log(this.fechaelegi);
    console.log(this.fechaelegi2);
    console.log(this.fecha2 + 'Esta es fechat 2 getTime');
    // convirtiendo a date los microsegundis nos da una fecha
    console.log(new Date(this.fecha3) + 'Esta es fechat 3 en date');
  }

  this.fecha1();
}
async noti(id,f,e) {
  const notifs = await LocalNotifications.schedule({
    notifications: [
      {
        title: "Recordatorio de tu cita",
        body: "Tienes una cita asignada el día de hoy",
        id: id,
        schedule: { at: new Date(this.fecha3 ) },
        sound: null,
        attachments: null,
        extra: {
          route: ['/tabs/tab1/mapa-cita/'+String(f)+'/'+e+'/'+id],
        },
        iconColor: '#BD008C',
      }
    ]
  });
  console.log('scheduled notifications', notifs);
  console.log(new Date());
  console.log('Se ha guardado tu notificacion con el id: ' + id);
}


fecha1(){
  const fechainicio =  moment(this.fechainicio + ' ' + this.horainicial).format();
  const fechaelegida =  moment(this.fechafinal + ' ' + this.horafinal).format();
  if (fechainicio > fechaelegida) {
    this.fechayhorafinal = '';
  } else {
    this.fechayhorafinal = (moment(this.fechafinal).format('dddd DD [de] MMMM [de] YYYY')) + ' ' + this.horafinal;
  }
  console.log(this.negado + " Hola sergiooooooooo");
}
crearcita(){
    console.log('direccion ' + this.direccion + 'Motivo ' + this.Motivo + 'fecha i' + this.fechainicio +
    ' hora i ' + this.horainicial + 'fecha f' + this.fechafinal + ' hora f' + this.horafinal +
    'usuario' + this.store + 'recordatorio' + this.idrecordatorio + 'empresa' + this.idempresa);
    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { 'caso': 4, 'direccion': this.direccion, 'motivo': this.Motivo, 'fechai': this.fechainicio,
    'fechaf': this.fechafinal, 'horai': this.horainicial, 'horaf': this.horafinal, 'idSO': this.idSO, 'idUsuario': this.store,
    'recordatorio': this.idrecordatorio, 'idEmpresa': this.idempresa};
    const URL: any = this.baseURL + 'calendario.php';
    let fechaInicial = this.fechainicio.concat(this.horainicial.toString());
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        if (this.archivos.length > 0) {
          const idcita = respuesta.toString();
          console.log(idcita);
          this.servidorarchivo(idcita, this.idempresa);
          this.Confirma();
          this.noti(idcita, fechaInicial, this.nombreE2);
        } else {
          this.Confirma();
          const idcita = respuesta.toString();
          this.noti(idcita, fechaInicial, this.nombreE2);
          console.log(idcita);
        }
      });
  }
  servidorarchivo(idcita, idE) {
    const formData = new FormData();
    for (var i = 0; i < this.archivos.length; i++) {
      formData.append("file[]", this.archivos[i]);
    }
    formData.append('idcita', idcita);
    formData.append('idtarea', '0');
    formData.append('idEmp', idE);

    this.subirarchivo.enviararchivo(formData).subscribe((resp) => {
      if (resp.toString() !== "") {
        this.presentAlertrue();
        this.nombredearchivos = [];
        this.archivos = [];
      } else {
        console.log("ocurrio un error");
        this.nombredearchivos = [];
        this.archivos = [];
      }
    });
  }
  async presentAlertrue() {
    const alert = await this.alertController.create({
      cssClass: 'alertclass',
      header: 'Se guardo la cita correctamente',
      buttons: ['Entendido']
    });
    await alert.present();
  }

  async Confirma() {
    const alert = await this.alertController.create({
      cssClass: 'alertclass',
      header: 'Se creó con exito!',
      buttons: [
         {
          text: 'Entendido',
          handler: () => {
            this.router.navigate(['/tabs/tab1']);
          }
        }
      ]
    });

    await alert.present();
  }


}
