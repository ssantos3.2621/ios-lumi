import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NuevaCitaoutPage } from './nueva-citaout.page';

describe('NuevaCitaoutPage', () => {
  let component: NuevaCitaoutPage;
  let fixture: ComponentFixture<NuevaCitaoutPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevaCitaoutPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NuevaCitaoutPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
